import vuetify from 'vuetify'
import Vue from 'vue'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import VuetifyTheme from './config/VuetifyTheme'
import { addDecorator } from '@storybook/vue'

Vue.use(vuetify, VuetifyTheme)

addDecorator(() => ({
  vuetify,
  template: `
    <v-app>
      <v-main>
        <v-container fluid >
          <story/>
        </v-container>
      </v-main>
    </v-app>
    `,
}))
