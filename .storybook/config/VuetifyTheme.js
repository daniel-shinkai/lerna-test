export default {
    theme: {
        primary: '#6d2080',
        secondary: '#FFFFFF',
        info: '#4F8CE9',
        alert: '#FFC040',
        gray: '#F1F1F1',
        warning: '#F19F54',
        error: '#FF7F63',
        success: '#02AE99',
        disabled: '#8D8D8D',
        turquoise: '#14ACCC',
        jade: '#02BA7F',
        'light-grey': '#DFDFDF',
        tangerine: '#fdb713',
        serpent: '#4F8CE9',
        imperial: '#214b6d',
        empty: '#E8ECF1',
        'mova-blue': '#375788',
        'infoLight': '#4B78BA',
        'green_parcela': '#02BA7F',
        'ec_analise': '#C50099'
    }        
}