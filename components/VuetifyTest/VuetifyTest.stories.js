import { storiesOf } from '@storybook/vue'
import VuetifyTest from './VuetifyTest'

storiesOf('VuetifyTest', module)
  .add('normal', () => ({
    components: { VuetifyTest },
    template:
    `
      <VuetifyTest 
        :values="values"
      />
    `,
    data: () => ({
      values: [
        'José Silva',
        'email@email.com'
      ]
    })
  }))